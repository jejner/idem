========
idem doc
========

The ``idem doc`` subcommand has prints function documentation for references in the code.
It will return all metadata about a function reference on the hub.
It uses the `pop-tree <https://gitlab.com/vmware/pop/pop-tree>`__ project under the hood to parse references on the hub.

Running the ``idem doc`` subcommand will return all references that match the given reference.

.. code-block:: bash

    idem doc <function reference on the hub>

Each returned reference will contain the following values.

doc
---

The function docstring.

file
----

The file that owns this particular reference on the hub.

start_line_number
-----------------

The line number in the file where the function begins, useful for `pyls-pop <https://gitlab.com/vmware/pop/pyls-pop>`_.

end_line_number
---------------

The line number in the file where the function ends, useful for `pyls-pop <https://gitlab.com/vmware/pop/pyls-pop>`_.

ref
---

The reference to this function on the hub

contracts
---------

There are three kinds of contracts, each function lists the references to all contracts that it implements.

- pre: A list of the function's ``pre`` contracts
- post: A list of the function's ``post`` contracts
- call: A list containing a function's ``call`` contract

parameters
----------

Each parameter in the function header is listed.
There are two possible values for a parameter:

- default: If this key is present for a parameter, it contains it's default value
- annotation: The typehint for a parameter if one exists

Examples
========

Get the documentation from a specific exec module function.

.. code-block:: bash

    idem doc exec.test.ping

output:

.. code-block::

    exec.test.ping:
    ----------
    doc:
        Immediately return success
    file:
        ~/PycharmProjects/idem/idem/exec/test.py
    start_line_number:
        13
    end_line_number:
        15
    ref:
        exec.test.ping
    contracts:
        ----------
        pre:
        call:
            - exec.recursive_contracts.soft_fail.call
        post:
            - exec.recursive_contracts.init.post
    parameters:
        ----------
        hub:
            ----------

Get the documentation for a specific state module function.

.. code-block:: bash

    idem doc states.test.present

output:

.. code-block::

    states.test.present:
    ----------
    doc:
        Return the previous old_state and the given new_state.
        Raise an error on fail
    file:
        ~/PycharmProjects/idem/idem/states/test.py
    start_line_number:
        279
    end_line_number:
        295
    ref:
        states.test.present
    contracts:
        ----------
        pre:
            - states.recursive_contracts.init.pre
        call:
        post:
            - states.recursive_contracts.resource.post_present
            - states.recursive_contracts.init.post
    parameters:
        ----------
        hub:
            ----------
        ctx:
            ----------
        name:
            ----------
            annotation:
                <class 'str'>
        new_state:
            ----------
            default:
                None
        result:
            ----------
            default:
                True
        force_save:
            ----------
            default:
                None

Return all functions in a single module:

.. code-block:: bash

    idem doc states.aws.ec2.vpc

Return all functions in a sub:

.. code-block:: bash

    idem doc states.aws

Return absolutely every reference on the hub:

.. code-block:: bash

    idem doc
