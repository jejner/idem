#!/usr/bin/env python3
import contextlib
import multiprocessing
import os
import pathlib
import sys

import idem.scripts

try:
    import tiamatpip.cli
    import tiamatpip.configure
    import tiamatpip.utils

    HAS_LIBS = True
except ImportError:
    HAS_LIBS = False


if __name__ == "__main__":
    running_from_compiled_binary = getattr(sys, "frozen", False)
    if HAS_LIBS and running_from_compiled_binary:
        if "TIAMAT_PIP_PYPATH" in os.environ:
            PIP_PATH = pathlib.Path(os.environ["TIAMAT_PIP_PYPATH"]).resolve()
        elif sys.platform.startswith("win"):
            PIP_PATH = pathlib.Path(os.getenv("LocalAppData"), "idem", "pypath")
        elif sys.platform.startswith("darwin"):
            PIP_PATH = pathlib.Path(
                os.getenv("HOME"),
                "Library",
                "Application Support",
                "idem",
                "pypath",
            )
        else:
            PIP_PATH = pathlib.Path(f"{os.sep}opt", "idem", "pypath")
        with contextlib.suppress(PermissionError):
            PIP_PATH.mkdir(mode=0o755, parents=True, exist_ok=True)
        tiamatpip.configure.set_user_base_path(PIP_PATH)
        if sys.platform.startswith("win"):
            multiprocessing.freeze_support()
        if tiamatpip.cli.should_redirect_argv(sys.argv):
            tiamatpip.cli.process_pip_argv(sys.argv)
        with tiamatpip.utils.patched_sys_argv(sys.argv):
            idem.scripts.start()
    else:
        idem.scripts.start()
