import pytest


def test_warning(hub):
    with pytest.warns(
        DeprecationWarning,
        match="Running 'describe' without implementing the `resource` contract",
    ):
        hub.pop.Loop.run_until_complete(hub.idem.describe.run("test.warning"))
