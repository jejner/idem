import ast

import pytest_idem.runner as runner


def test_cli(idem_cli):
    # Run the describe command
    yaml = idem_cli("describe", "test", "--output=yaml", check=True)
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(yaml.stdout)

        # Verify that passing states were created from it
        data = idem_cli("state", fh, "--runtime=serial", check=True).json

    assert "test_|-Description of test.absent_|-absent_|-absent" in data
    assert "test_|-Description of test.present_|-present_|-present" in data


def test_cli_regex(idem_cli):
    # Run the describe command
    yaml = idem_cli("describe", "te.*", "--output=yaml", check=True)
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(yaml.stdout)

        # Verify that passing states were created from it
        data = idem_cli("state", fh, "--runtime=serial", check=True).json

    assert "test_|-Description of test.absent_|-absent_|-absent" in data
    assert "test_|-Description of test.present_|-present_|-present" in data
    assert (
        "test_regex_|-Description of regex test_regex.none_without_changes_regex_|-none_without_changes_regex_|-none_without_changes_regex"
        in data
    )
    assert (
        "test_regex_|-Description of regex test_regex.succeed_without_changes_regex_|-succeed_without_changes_regex_|-succeed_without_changes_regex"
        in data
    )

    assert (
        "None describe regular expression!"
        in data[
            "test_regex_|-Description of regex test_regex.none_without_changes_regex_|-none_without_changes_regex_|-none_without_changes_regex"
        ]["comment"]
    )
    assert (
        "Success describe regular expression!"
        in data[
            "test_regex_|-Description of regex test_regex.succeed_without_changes_regex_|-succeed_without_changes_regex_|-succeed_without_changes_regex"
        ]["comment"]
    )


def test_jmespath_output(idem_cli):
    data = idem_cli("describe", "test", "--output=jmespath", check=True)

    assert ast.literal_eval(data.stdout) == [
        {
            "name": "Description of test.absent",
            "ref": "test.absent",
            "resource": [
                {"name": "absent"},
                {"old_state": None},
                {"changes": None},
                {"new_state": None},
                {"result": True},
                {"force_save": None},
            ],
        },
        {
            "name": "Description of test.present",
            "ref": "test.present",
            "resource": [
                {"name": "present"},
                {"old_state": None},
                {"changes": None},
                {"new_state": None},
                {"result": True},
                {"force_save": None},
            ],
        },
    ]


def test_jmespath_filter(idem_cli):
    data = idem_cli(
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "@",
        check=True,
    )

    assert ast.literal_eval(data.stdout) == [
        {
            "name": "Description of test.absent",
            "ref": "test.absent",
            "resource": [
                {"name": "absent"},
                {"old_state": None},
                {"changes": None},
                {"new_state": None},
                {"result": True},
                {"force_save": None},
            ],
        },
        {
            "name": "Description of test.present",
            "ref": "test.present",
            "resource": [
                {"name": "present"},
                {"old_state": None},
                {"changes": None},
                {"new_state": None},
                {"result": True},
                {"force_save": None},
            ],
        },
    ]


def test_filter(idem_cli):
    data = idem_cli(
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "[?resource[?name=='present']]",
        check=True,
    )

    assert ast.literal_eval(data.stdout) == [
        {
            "name": "Description of test.present",
            "ref": "test.present",
            "resource": [
                {"name": "present"},
                {"old_state": None},
                {"changes": None},
                {"new_state": None},
                {"result": True},
                {"force_save": None},
            ],
        }
    ]


def test_describe_error(tests_dir, idem_cli):
    ret = idem_cli("state", tests_dir / "sls" / "describe.sls", check=False)

    assert (
        "'describe' functions should only be called by the describe subcommand."
        in ret.stdout
    )
