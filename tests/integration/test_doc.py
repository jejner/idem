def test_exec_doc(tree, idem_cli):
    ref = "exec.test.ping"
    data = idem_cli("doc", ref, check=True).json

    assert data[ref]["doc"] == "Immediately return success"
    assert data[ref]["file"].endswith("test.py"), data[ref]["file"]
    assert data[ref]["ref"] == ref
    assert "parameters" in data[ref]
    assert "contracts" in data[ref]
    assert "start_line_number" in data[ref]
    assert "end_line_number" in data[ref]


def test_state_doc(tree, idem_cli):
    ref = "states.test.present"
    data = idem_cli("doc", ref, check=True).json

    assert (
        data[ref]["doc"]
        == "Return the previous old_state, if it's not specified in sls, and the given new_state.\nRaise an error on fail"
    )
    assert data[ref]["file"].endswith("test.py"), data[ref]["file"]
    assert data[ref]["ref"] == ref
    assert "parameters" in data[ref]
    assert "contracts" in data[ref]
    assert "start_line_number" in data[ref]
    assert "end_line_number" in data[ref]


def test_full_doc(tree, idem_cli):
    data = idem_cli("doc", check=True)

    # We're not going to verify the documentation of every single function on the hub
    # It's enough that the command was successful and returned more than one thing
    assert len(data) > 1
