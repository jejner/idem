import pytest
from pytest_idem.runner import run_sls


def test_render_empty():
    """Test that Idem raises a user-friendly warning when an sls is empty after rendering"""
    empty_sls = "empty"
    with pytest.warns(
        RuntimeWarning,
        match=r"SLS ref 'empty' is not resolved to any state.",
    ):
        # This empty file is not empty content. But after Jinjia rendering, the rendered result is None
        run_sls([empty_sls])


def test_render_empty_and_valid():
    """Test that Idem raises a user-friendly warning when an sls is empty after rendering, and continues to process
    other sls files"""
    empty_sls = "empty"
    with pytest.warns(
        RuntimeWarning,
        match=r"SLS ref 'empty' is not resolved to any state.",
    ):
        ret = run_sls([empty_sls, "simple"])
        assert ret["test_|-happy_|-happy_|-nop"]["result"] is True
        assert ret["test_|-sad_|-sad_|-fail_without_changes"]["result"] is False
