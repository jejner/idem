import pop.hub
import pop.loader


def test_contracts_separated_resolver(capsys):
    # Set up the hub like idem does
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.loop.create()

    def resolve(path, context):
        return hub.exec.test.ctx

    hub.pop.sub.dynamic(resolve, None, dyne_name="mod", sub=hub.exec)

    ret = hub.pop.Loop.run_until_complete(
        hub.idem.ex.run("exec.mod.foo.bar", args=[], kwargs={})
    )
    assert ret.result
    assert ret.ret == {"acct": {}, "acct_details": {}, "test": False}
    assert ret.ref == "exec.test.ctx_"
