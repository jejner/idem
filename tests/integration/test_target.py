def test_single_target(idem_cli, tests_dir, mock_time):
    # Specify a target resource,
    # only the target and its requisites should be invoked,
    # in this case the target and 2 dependent resources.
    output = idem_cli(
        "state",
        tests_dir / "sls" / "target.sls",
        "--target=the_target",
        check=True,
    ).json

    assert 3 == len(output), len(output)
    output["test_|-the_target_|-the_target_|-succeed_without_changes"].pop("start_time")
    output["test_|-the_target_|-the_target_|-succeed_without_changes"].pop(
        "total_seconds"
    )

    output["test_|-dep_2_|-dep_2_|-nop"].pop("start_time")
    output["test_|-dep_2_|-dep_2_|-nop"].pop("total_seconds")
    assert output["test_|-dep_2_|-dep_2_|-nop"] == {
        "__run_num": 1,
        "changes": {},
        "comment": "Success!",
        "esm_tag": "test_|-dep_2_|-dep_2_|-",
        "name": "dep_2",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "tag": "test_|-dep_2_|-dep_2_|-nop",
    }

    output["test_|-dep_1_|-dep_1_|-nop"].pop("start_time")
    output["test_|-dep_1_|-dep_1_|-nop"].pop("total_seconds")
    assert output["test_|-dep_1_|-dep_1_|-nop"] == {
        "__run_num": 2,
        "changes": {},
        "comment": "Success!",
        "esm_tag": "test_|-dep_1_|-dep_1_|-",
        "name": "dep_1",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "tag": "test_|-dep_1_|-dep_1_|-nop",
    }

    assert output["test_|-the_target_|-the_target_|-succeed_without_changes"] == {
        "__run_num": 3,
        "changes": {},
        "comment": "Success!",
        "esm_tag": "test_|-the_target_|-the_target_|-",
        "name": "the_target",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "tag": "test_|-the_target_|-the_target_|-succeed_without_changes",
    }


def test_target_with_name_property(idem_cli, tests_dir, mock_time):
    # Specify the declaration ID as the 'target',
    # which might be different than the resource name.
    output = idem_cli(
        "state", tests_dir / "sls" / "target.sls", "--target=another_target", check=True
    ).json

    assert 1 == len(output), len(output)
    output["test_|-another_target_|-the_name_|-succeed_without_changes"].pop(
        "start_time"
    )
    output["test_|-another_target_|-the_name_|-succeed_without_changes"].pop(
        "total_seconds"
    )

    assert output["test_|-another_target_|-the_name_|-succeed_without_changes"] == {
        "__run_num": 1,
        "changes": {},
        "comment": "Success!",
        "esm_tag": "test_|-another_target_|-the_name_|-",
        "name": "the_name",
        "new_state": None,
        "old_state": None,
        "rerun_data": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "tag": "test_|-another_target_|-the_name_|-succeed_without_changes",
    }


def test_target_negative(idem_cli, tests_dir, mock_time):
    # Specify the name of the resource, which is different from the
    # property name. It should fail.
    ret = idem_cli(
        "state",
        tests_dir / "sls" / "target.sls",
        "--target=the_name",
        "--output=state",
        check=False,
    ).stdout
    assert "Invalid 'target' for run 'cli': the_name." in ret


def test_nested_resources(idem_cli, tests_dir, mock_time):
    # Target is a declaration ID that has multiple nested resources
    # All will be invoked with their dependencies
    output = idem_cli(
        "state",
        tests_dir / "sls" / "target.sls",
        "--target=nested_resources",
        check=True,
    ).json

    assert 4 == len(output), len(output)
    assert output.get("test_|-nested_resources_|-test_nested_|-succeed_without_changes")
    assert output.get("test_|-dep_2_|-dep_2_|-nop")
    assert output.get("test_|-dep_1_|-dep_1_|-nop")
    assert output.get(
        "test_regex_|-nested_resources_|-nested_resources_|-none_without_changes_regex"
    )
