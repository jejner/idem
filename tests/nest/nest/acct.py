import json


def __init__(hub):
    hub.nest.ACCT = ["test_acct"]


async def gather(hub, name, ctx):
    return {
        "name": name,
        "result": True,
        "changes": {},
        "comment": json.dumps(ctx["acct"]),
    }
