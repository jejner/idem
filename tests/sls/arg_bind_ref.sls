first thing:
  test.succeed_with_changes

arg bind ref:
  test.succeed_with_arg_bind:
    - parameters:
        test1: First - ${test:first thing:testing:new}. Second - ${test:first thing:tests[0][0]:new}. Finished
        test2: ${test:first thing:tests[0][0]:new}
        test3:
          - ${test:third thing:testing:new}
          - ${test:third thing:tests[0][0]:new} -- ${test:third thing:tests[0][0]:new}
        test4: ${test:first thing:testing}

multi-line ref:
  test.succeed_with_arg_bind:
    - parameters: '{
                       "Id": "key-consolepolicy-3",
                         {
                           "Sid": "Allow access for Key Administrators",
                           "Effect": "Allow",
                           "Principal": {
                             "AWS": [
                               "${test:first thing:tests[0][0]:new}",
                               "${test:first thing:tests[0][0]:new}"
                             ]
                           }'

third thing:
  test.succeed_with_changes

fail reference format:
  test.succeed_with_arg_bind:
    - parameters:
        test1: ${testing:new}

reference within list:
    test.succeed_with_arg_bind:
     - top_list:
        - first_element:
        - second_element:
          inner_list:
            - a: ${test:first thing:tests[0][0]:new}
            - b: ${test:third thing:testing:new}

arg bind dict key:
  test.succeed_with_arg_bind:
    - parameters:
        test1[0][1]: ${test:first thing:testing:new}

multi_result_new_state:
  test.present:
    - result: True
    - changes: 'skip'
    - new_state:
        - "resource_id": "s1"
          "name": "subnet1"
        - "resource_id": "s2"
          "name": "subnet2"

arg_bind_new_state_collection:
  test.succeed_with_arg_bind:
    - parameters:
        - ${test:multi_result_new_state:[0]:resource_id}
        - ${test:multi_result_new_state:[1]:resource_id}

arg_bind_wildcard:
  test.succeed_with_arg_bind:
    - parameters:
        ${test:multi_result_new_state:[*]:resource_id}

arg_bind_wildcard_error:
  test.succeed_with_arg_bind:
    - parameters:
        ${test:multi_result_new_state:[*][*]}
