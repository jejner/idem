esm_test_state_ref:
  test.present:
    - result: True
    - new_state:
        key1: ${test:esm_test_state:key}
        name_ref: ${test:esm_test_state:name}


esm_test_state_ref_fail:
  test.present:
    - result: True
    - new_state:
        key1: ${test:esm_test_state_fail:key}
